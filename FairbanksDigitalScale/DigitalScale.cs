﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FairbanksDigitalScale
{
    public class DigitalScale
    {
        decimal? weightInLb, weightInG, weightInOz, weight;
        bool? isStable;
        string scaleStatus, weightUnit;
        USBScale s = new USBScale();
        public Scale_DTO Connect()
        {
            Scale_DTO scale = new Scale_DTO();

            s.Connect();
            if (s.IsConnected)
            {
                s.GetWeight(out weight, out weightInLb, out weightInG, out weightInOz, out isStable, out scaleStatus, out weightUnit);
                // s.DebugScaleData();
                scale.weight = weight.ToString();
                scale.units = weightUnit;
                scale.isStable = (bool)isStable ? "true" : "false";
                scale.scaleStatus = scaleStatus;
                scale.message = "Connected";
                scale.status = "OK";
            }
            else
            {
                scale.message = "No Scale Connected";
                scale.message = "Failed";
            }
            return scale;
        }

        public Scale_DTO Sample()
        {
            Scale_DTO scale = new Scale_DTO();
            s.GetWeight(out weight, out weightInLb, out weightInG, out weightInOz, out isStable, out scaleStatus, out weightUnit);
            scale.weight = weight.ToString();
            scale.units = weightUnit;
            scale.isStable = (bool)isStable ? "true" : "false";
            scale.scaleStatus = scaleStatus;
            scale.message = "Connected";
            scale.status = "OK";
            return scale;
        }

        public void Disconnect()
        {
            s.Disconnect();
        }

        public delegate void ConnectAsyncCallbackDelegate(Scale_DTO scale);

        public void ConnectCallback(ConnectAsyncCallbackDelegate callback)
        {
            Scale_DTO scale = new Scale_DTO();
            s.Connect();

            if (s.IsConnected)
            {
                scale = GetWeight();
            }
            else
            {
                scale.message = "No Scale Connected";
                scale.message = "Failed";
            }
            callback(scale);
        }

        public delegate void SampleAsyncCallbackDelegate(Scale_DTO scale);

        public void SampleCallback(SampleAsyncCallbackDelegate callback)
        {
            callback(GetWeight());
        }

        protected Scale_DTO GetWeight()
        {
            Scale_DTO scale = new Scale_DTO();
            var _weight = s.GetScaleWeight();

            s.GetWeight(out weight, out weightInLb, out weightInG, out weightInOz, out isStable, out scaleStatus, out weightUnit);
            scale.weight = weight.ToString();
            scale.units = weightUnit;
            scale.isStable = (bool)isStable ? "true" : "false";
            scale.scaleStatus = scaleStatus;
            scale.message = "Connected";
            scale.status = "OK";
            return scale;
        }
    }
}
