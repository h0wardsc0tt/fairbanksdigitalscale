﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FairbanksDigitalScale
{
    [Serializable]
    public class Scale_DTO
     {
        [DataMember]
        public string scaleStatus { get; set; }
        [DataMember]
        public string weight { get; set; }
        [DataMember]
        public string units { get; set; }
        [DataMember]
        public string isStable { get; set; }
        [DataMember]
        public string message { get; set; }
        [DataMember]
        public string status { get; set; }
    }
}
