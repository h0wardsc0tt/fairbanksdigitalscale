﻿using HidLibrary;
using System.Threading;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;

namespace FairbanksDigitalScale
{
    class USBScale
    {
        public bool IsConnected
        {
            get
            {
                return scale == null ? false : scale.IsConnected;
            }
        }
        public decimal ScaleStatus
        {
            get
            {
                return inData.Data[1];
            }
        }
        public decimal ScaleWeightUnits
        {
            get
            {
                return inData.Data[2];
            }
        }
        private HidDevice scale;
        private HidDeviceData inData;

        public HidDevice[] GetDevices()
        {
            //The next line contains the product/vendor ID numbers for the Dymo 25lb Postal Scale, change these depdending on what scale you're using 
            // return HidDevices.Enumerate(0x0922, 0x8004).Cast<HidDevice>().ToArray();
            return HidDevices.Enumerate(0x0B67, 0x555E).Cast<HidDevice>().ToArray();
        }

        public bool Connect()
        {
            // Find a Scale
            HidDevice[] deviceList = GetDevices();

            if (deviceList.Length > 0)

                return Connect(deviceList[0]);

            else

                return false;
        }

        public bool Connect(HidDevice device)
        {
            scale = device;
            int waitTries = 0;
            scale.OpenDevice();

            // sometimes the scale is not ready immedietly after
            // Open() so wait till its ready
            while (!scale.IsConnected && waitTries < 10)
            {
                Thread.Sleep(50);
                waitTries++;
            }
            return scale.IsConnected;
        }

        public void Disconnect()
        {
            if (scale.IsConnected)
            {
                scale.CloseDevice();
                scale.Dispose();
            }
        }

        //public void DebugScaleData()
        //{
        //    for (int i = 0; i < inData.Data.Length; ++i)
        //    {
        //        Console.WriteLine("Byte {0}: {1}", i, inData.Data[i]);
        //    }
        //    Console.WriteLine(Convert.ToDecimal(inData.Data[4]));
        //    Console.WriteLine(Convert.ToDecimal(inData.Data[5]) * 256);
        //    Console.WriteLine(Convert.ToDecimal(inData.Data[5]));
        //}

        private async Task<HidDeviceData> asyncGetWeight()
        {
            inData = await scale.ReadAsync();
            return inData;

        }

        public async Task<ScaleWeight> GetScaleWeight()
        {
            inData = await asyncGetWeight();
            ScaleWeight weight = new ScaleWeight();
            return weight;

        }

        public void GetWeight(out decimal? weight, out decimal? weightInLb, out decimal? weightInG, out decimal? weightInOz, out bool? isStable, out string scaleStatus, out string weightUnit)
        {
            //decimal? weight;
            weight = null;
            weightInG = null;
            weightInLb = null;
            weightInOz = null;
            isStable = false;
            scaleStatus = string.Empty;
            weightUnit = string.Empty;

            if (scale.IsConnected)
            {
                //inData = await asyncGetWeight();
                inData = scale.Read(250);
                // Byte 0 == Report ID?
                // Byte 1 == Scale Status (1 == Fault, 2 == Stable @ 0, 3 == In Motion, 4 == Stable, 5 == Under 0, 6 == Over Weight, 7 == Requires Calibration, 8 == Requires Re-Zeroing)
                // Byte 2 == Weight Unit
                // Byte 3 == Data Scaling (decimal placement)
                // Byte 4 == Weight LSB
                // Byte 5 == Weight MSB

                // FIXME: dividing by 100 probably wont work with
                // every scale, need to figure out what to do with
                // Byte 3
                weight = (Convert.ToDecimal(inData.Data[4]) +
                    Convert.ToDecimal(inData.Data[5]) * 256) / 100;
                switch (Convert.ToInt16(inData.Data[2]))
                {
                    case 3:  // Kilos
                        //weight = weight * (decimal?)2.2;
                        break;
                    case 11: // Ounces
                        //weight = weight * (decimal?)0.625;
                        break;
                    case 12: // Pounds
                        // already in pounds, do nothing
                        break;
                }

                isStable = inData.Data[1] == 0x4;

                switch (String.Format("{0}", inData.Data[2]))
                {
                    case "3":
                        weightUnit = "kb";
                        break;
                    case "12":
                        weightUnit = "lb";
                        break;
                    case "11":
                        weightUnit = "oz";
                        break;
                }

                switch (String.Format("{0}", inData.Data[1]))
                {
                    case "1":
                        scaleStatus = "Fault";
                        break;
                    case "2":
                        scaleStatus = "Stable";
                        break;
                    case "3":
                        scaleStatus = "In Motion";
                        break;
                    case "4":
                        scaleStatus = "Stable";
                        break;
                    case "5":
                        scaleStatus = "Under 0";
                        break;
                    case "6":
                        scaleStatus = "Over Weight";
                        break;
                    case "7":
                        scaleStatus = "Over Weight";
                        break;
                    case "8":
                        scaleStatus = "Requires Re-Zeroing";
                        break;
                }
            }
        }
    }

    public class ScaleWeight
    {
        public decimal? weight { get; set; }
        public decimal? weightInLb { get; set; }       
        public decimal? weightInG { get; set; }
        public decimal? weightInOz { get; set; }
        public bool? isStable { get; set; }      
        public string scaleStatus { get; set; }      
        public string weightUnit { get; set; }
    }
}