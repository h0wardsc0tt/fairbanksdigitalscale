﻿namespace FairbanksDigitalScaleClient
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.scaleStatus = new System.Windows.Forms.TextBox();
            this.lable_scaleStatus = new System.Windows.Forms.Label();
            this.weight = new System.Windows.Forms.TextBox();
            this.completed = new System.Windows.Forms.Label();
            this.stopButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // scaleStatus
            // 
            this.scaleStatus.Location = new System.Drawing.Point(117, 44);
            this.scaleStatus.Name = "scaleStatus";
            this.scaleStatus.Size = new System.Drawing.Size(129, 20);
            this.scaleStatus.TabIndex = 0;
            // 
            // lable_scaleStatus
            // 
            this.lable_scaleStatus.AutoSize = true;
            this.lable_scaleStatus.Location = new System.Drawing.Point(44, 47);
            this.lable_scaleStatus.Name = "lable_scaleStatus";
            this.lable_scaleStatus.Size = new System.Drawing.Size(70, 13);
            this.lable_scaleStatus.TabIndex = 1;
            this.lable_scaleStatus.Text = "Scale Status:";
            // 
            // weight
            // 
            this.weight.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.weight.Location = new System.Drawing.Point(47, 134);
            this.weight.Name = "weight";
            this.weight.ReadOnly = true;
            this.weight.Size = new System.Drawing.Size(199, 53);
            this.weight.TabIndex = 3;
            this.weight.Text = "20.00 lb";
            this.weight.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // completed
            // 
            this.completed.AutoSize = true;
            this.completed.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.completed.ForeColor = System.Drawing.Color.Red;
            this.completed.Location = new System.Drawing.Point(85, 87);
            this.completed.Name = "completed";
            this.completed.Size = new System.Drawing.Size(0, 25);
            this.completed.TabIndex = 4;
            // 
            // stopButton
            // 
            this.stopButton.Location = new System.Drawing.Point(99, 209);
            this.stopButton.Name = "stopButton";
            this.stopButton.Size = new System.Drawing.Size(75, 23);
            this.stopButton.TabIndex = 5;
            this.stopButton.Text = "Submit";
            this.stopButton.UseVisualStyleBackColor = true;
            this.stopButton.Visible = false;
            this.stopButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.stopButton);
            this.Controls.Add(this.completed);
            this.Controls.Add(this.weight);
            this.Controls.Add(this.lable_scaleStatus);
            this.Controls.Add(this.scaleStatus);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox scaleStatus;
        private System.Windows.Forms.Label lable_scaleStatus;
        private System.Windows.Forms.TextBox weight;
        private System.Windows.Forms.Label completed;
        private System.Windows.Forms.Button stopButton;
    }
}

