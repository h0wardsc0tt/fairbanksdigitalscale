﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FairbanksDigitalScale;

namespace FairbanksDigitalScaleClient
{
     public partial class Form1 : Form
    {
        bool exit = false;
        bool callback = true;
        FairbanksDigitalScale.DigitalScale ds = new FairbanksDigitalScale.DigitalScale();
        public Form1()
        {
            InitializeComponent();
           // Load += new EventHandler(OnLoad);
            this.Shown += new System.EventHandler(this.onLoad);
            this.FormClosing += Form1_FormClosing;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            exit = true;
            ds.Disconnect();
            Application.Exit();
        }

        private void onLoad(object sender, EventArgs e)
        {
            FairbanksDigitalScale.Scale_DTO dto = ds.Connect();
            this.weight.Text = dto.weight + "  " + dto.units;
            this.scaleStatus.Text = dto.scaleStatus;
            weight.Refresh();
            scaleStatus.Refresh();
            if (callback)
            {
                ds.SampleCallback(SampleCallback);
            }
            else
            {
                while (!exit)
                {
                    Application.DoEvents();
                    if (dto.scaleStatus != "Stable" || dto.weight == "0")
                    {
                        stopButton.Visible = false;
                    }
                    else
                    {
                        stopButton.Visible = true;

                    }
                    stopButton.Refresh();
                    Application.DoEvents();
                    dto = ds.Sample();
                    Application.DoEvents();
                    this.weight.Text = dto.weight + "  " + dto.units;
                    this.scaleStatus.Text = dto.scaleStatus;
                    Application.DoEvents();
                    weight.Refresh();
                    scaleStatus.Refresh();
                    System.Threading.Thread.Sleep(100);
                    Application.DoEvents();
                }
            }
         }

        private void SampleCallback(Scale_DTO dto)
        {
            if (exit) return;
            Application.DoEvents();
            if (dto.scaleStatus != "Stable" || dto.weight == "0")
            {
                stopButton.Visible = false;
            }
            else
            {
                stopButton.Visible = true;

            }
            stopButton.Refresh();
            weight.Text = dto.weight + "  " + dto.units;
            scaleStatus.Text = dto.scaleStatus;
            weight.Refresh();
            scaleStatus.Refresh();
            System.Threading.Thread.Sleep(100);

            ds.SampleCallback(SampleCallback);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            exit = true;
            Application.DoEvents();
            ds.Disconnect();
            Application.DoEvents();
            completed.Text = "Completed";
            completed.Refresh();
        }
    }
}
